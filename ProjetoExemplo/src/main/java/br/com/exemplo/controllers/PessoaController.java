package br.com.exemplo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.exemplo.entities.Pessoa;
import br.com.exemplo.services.PessoaService;

@RestController
@RequestMapping(path = "/pessoa")
public class PessoaController {
	
	private PessoaService servicePessoa;
	
	@Autowired
	public PessoaController(PessoaService servicePessoa) {
		this.servicePessoa = servicePessoa;
	}
	
	@GetMapping("/nome")
	public String dizNomeLuiz() {
		return "O nome da pessoa que criou é Luiz";
	}
	
	
	@PutMapping
	public Pessoa salvarPessoa(@RequestBody Pessoa pessoa) {
		return servicePessoa.salvarPessoa(pessoa);
	}
	
}
