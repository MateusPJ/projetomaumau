package br.com.exemplo.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.exemplo.entities.Pessoa;

public interface PessoaRepositorio extends JpaRepository<Pessoa, Long>{

	@Query("select p from Pessoa p where p.nome = :nome")
	public Pessoa findByNome(String nome);
}
