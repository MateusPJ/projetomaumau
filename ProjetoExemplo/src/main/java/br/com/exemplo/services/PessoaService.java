package br.com.exemplo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.exemplo.entities.Pessoa;
import br.com.exemplo.repositorios.PessoaRepositorio;

@Service
@Transactional
public class PessoaService {

	private PessoaRepositorio repositorio;
	
	@Autowired
	public PessoaService(PessoaRepositorio repositorio) {
		this.repositorio = repositorio;
	}
	
	public Pessoa findById(Long id) {
		return repositorio.findById(id).get();
	}
	
	public Pessoa salvarPessoa(Pessoa pessoaSalva) {
		return repositorio.save(pessoaSalva);
	}
}
